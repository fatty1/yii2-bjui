<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = '关于我们';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>为微信用户(非公众号)提供促销插件,例如大转盘 刮刮卡 抽奖 秒杀等促销活动插件</p>

</div>
