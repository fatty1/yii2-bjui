<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
$this->title = '微商促销平台';
?>
<div class="site-index">
    <div class="jumbotron">
        <h1>微商促销平台</h1>
        <p class="lead">代码托管到Git@OSC</p>
        <p>
            <a class="btn btn-lg btn-success" href="https://git.oschina.net/zhangchengbo/yii2-bjui">访问</a>
        </p>
    </div>
    <div class="body-content">
        <div class="row">
            <div class="col-lg-4">
                <h2>前台</h2>
                <p>前台采用yii2原生开发</p>
                <p>
                    <a class="btn btn-default" href="http://www.weixin.hnqyw.com/">访问 &raquo;</a>
                </p>
            </div>
            <div class="col-lg-4">
                <h2>后台</h2>
                <p>后台整合B-JUI</p>
                <p>
                    <a class="btn btn-default" href="http://admin.weixin.hnqyw.com/" target="_blank">访问 &raquo;</a>
                </p>
            </div>
            <div class="col-lg-4">
                <h2>接口</h2>
                <p>接口采用RESTFul标准</p>
                <p>
                    <a class="btn btn-default" href="http://api.weixin.hnqyw.com/" target="_blank">访问 &raquo;</a>
                </p>
            </div>
        </div>
    </div>
</div>
