<?php
return [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'D1t8YlnHXmK1THz9NdpBEpsN5feYCBj5',
        ],
    ],
];
