<?php
namespace frontend\controllers;

use yii\web\Controller;
use lib\wxbiz\Wechat;

class WxbizController extends Controller
{
    public function init(){
        $this->enableCsrfValidation = false;
    }

    public function actionIndex()
    {
        // 假设企业号在公众平台上设置的参数如下
        $token = \Yii::$app->params['token'];
        $encodingAesKey = \Yii::$app->params['encodingAesKey'];
        $corpId = \Yii::$app->params['corpId'];

        $wx = new Wechat($token, $encodingAesKey, $corpId);
//         var_dump($wx);
        $wx->run();
    }
}