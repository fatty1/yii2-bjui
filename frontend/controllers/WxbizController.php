<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use lib\wxbiz\Wechat;

class WxbizController extends Controller
{

    public function init()
    {
        $this->enableCsrfValidation = false;
    }

    public function actionIndex($id)
    {
        $wechat = new Wechat($id);
    }
}