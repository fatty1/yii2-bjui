<?php
namespace frontend\controllers;

use yii\web\Controller;

class CeshiController extends Controller
{

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\PageCache',
                'only' => [
                    'index'
                ],
                'duration' => 60,
                'variations' => [
                    \Yii::$app->language
                ],
                'dependency' => [
                    'class' => 'yii\caching\DbDependency',
                    'sql' => 'SELECT COUNT(*) FROM mxq_user'
                ]
            ]
        ];
    }

    public function actions()
    {
        return [];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
}
