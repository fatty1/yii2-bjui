<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use lib\wxbiz\Wechat;
use common\models\WxbizAgent;

class MemController extends Controller
{

    public function actionIndex($id)
    {
        $cache = Yii::$app->cache;
        $keys = array(
            'wxbiz.token.' . $id,
            'wxbiz.encodingAesKey.' . $id,
            'wxbiz.corpId.' . $id,
            'wxbiz.secret.' . $id
        );
        $items = $cache->mget($keys);
        if ($items['wxbiz.token.' . $id] === false) {
            $agent = WxbizAgent::findOne($id);
            // $agent = new WxbizAgent();
            $items = array(
                'wxbiz.token.' . $id => $agent->token . time(),
                'wxbiz.encodingAesKey.' . $id => $agent->encodingAesKey,
                'wxbiz.corpId.' . $id => $agent->corp->corpId,
                'wxbiz.secret.' . $id => $agent->role->secret
            );
            $cache->mset($items, 60);
        }
        list ($token, $encodingAesKey, $corpId, $secret) = array_values($items);
        $wechat = new Wechat($token, $encodingAesKey, $corpId);
        $wechat->run();
    }
}
