<?php
namespace service\controllers;

use Yii;
use yii\console\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{

    public function actionIndex()
    {
        return time();
    }

    public function actionError()
    {
        return 'error';
    }
}
