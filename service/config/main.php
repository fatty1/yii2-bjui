<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-service',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'service\controllers',
    'components' => [
//         'errorHandler' => [
//             'errorAction' => 'site/error',
//         ],
    ],
    'params' => $params,
];
