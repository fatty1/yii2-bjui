<?php
namespace lib\wxbiz;

use Yii;
use common\models\WxbizAgent;

class WechatCache
{

    /**
     * 从微信服务器获取微信 access_token
     *
     * @return Ambigous|bool
     */
    public static function getAccessToken($agent_id)
    {
        $cache = Yii::$app->cache;
        $access_token = $cache->get('wxbiz.access_token.' . $agent_id);
        if ($access_token === false) {
            $url = 'https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=' . self::getCorpId($agent_id) . '&corpsecret=' . self::getSecret($agent_id);
            $result = Curl::callWebServer($url, '', 'GET');
            if (isset($result['errcode'])) {
                die($result['errmsg']);
            } else {
                $access_token = $result['access_token'];
                $cache->set('wxbiz.access_token.' . $agent_id, $access_token, intval($result['expires_in']) - 10);
            }
        }
        return $access_token;
    }

    public static function getToken($agent_id)
    {
        $cache = Yii::$app->cache;
        $token = $cache->get('wxbiz.token.' . $agent_id);
        if ($token === false) {
            $model = self::findModel($agent_id);
            $token = $model->token;
            $cache->set('wxbiz.token.' . $agent_id, $token, 600);
        }
        return $token;
    }

    public static function getEncodingAesKey($agent_id)
    {
        $cache = Yii::$app->cache;
        $encodingAesKey = $cache->get('wxbiz.encodingAesKey.' . $agent_id);
        if ($encodingAesKey === false) {
            $model = self::findModel($agent_id);
            $encodingAesKey = $model->encodingAesKey;
            $cache->set('wxbiz.encodingAesKey.' . $agent_id, $encodingAesKey, 600);
        }
        return $encodingAesKey;
    }

    public static function getAgentid($agent_id)
    {
        $cache = Yii::$app->cache;
        $agentid = $cache->get('wxbiz.agentid.' . $agent_id);
        if ($agentid === false) {
            $model = self::findModel($agent_id);
            $agentid = $model->agentid;
            $cache->set('wxbiz.agentid.' . $agent_id, $agentid, 600);
        }
        return $agentid;
    }

    public static function getCorpId($agent_id)
    {
        $cache = Yii::$app->cache;
        $corpId = $cache->get('wxbiz.corpId.' . $agent_id);
        if ($corpId === false) {
            $model = self::findModel($agent_id);
            $corpId = $model->corp->corpId;
            $cache->set('wxbiz.corpId.' . $agent_id, $corpId, 600);
        }
        return $corpId;
    }

    public static function getSecret($agent_id)
    {
        $cache = Yii::$app->cache;
        $secret = $cache->get('wxbiz.secret.' . $agent_id);
        if ($secret === false) {
            $model = self::findModel($agent_id);
            $secret = $model->role->secret;
            $cache->set('wxbiz.secret.' . $agent_id, $secret, 600);
        }
        return $secret;
    }

    public static function getErrMsg($errcode)
    {
        $cache = Yii::$app->cache;
        $errmsg = $cache->get('wxbiz.errmsg');
        if ($errmsg === false) {
            $cache->set('wxbiz.errmsg', $errmsg, 600);
        }
        return $errmsg;
    }

    protected function findModel($agent_id)
    {
        if (($model = WxbizAgent::findOne($agent_id)) !== null) {
            return $model;
        } else {
            die(Msg::returnErrMsg('0', '应用不存在'));
        }
    }
}
?>