<?php
namespace lib\wxbiz;

class Message
{

    public static function Send($agent_id, $data)
    {
        $access_token = WechatCache::getAccessToken($agent_id);
        $url = 'https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=' . $access_token;
        return Curl::callWebServer($url, json_encode($data, JSON_UNESCAPED_UNICODE), 'POST');
    }
}