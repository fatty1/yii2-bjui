<?php use yii\helpers\Url;?>
<script type="text/javascript">
$('#user-datagrid').datagrid({
    gridTitle : '用户列表',
    showToolbar: true,
    toolbarItem: 'add, edit, |, del',
    local: 'remote',
    dataUrl: '<?= Url::toRoute('user/search') ?>',
    loadType: 'get',
    dataType: 'json',
    sortAll: true,
    filterAll: true,
    columns: [
        {
            name: 'username',
            label: '登录帐号',
        },
        {
            label: '客户信息',
            columns: [
            {
                name: 'mobile',
                label: '手机号码'
            },
            {
                name: 'email',
                label: '电子邮件'
            },
            {
                name: 'name',
                label: '姓名',
                align: 'center',
                width: 100
            },
            {
                name: 'sex',
                label: '性别',
                type: 'select',
                items: [{'true':'男'}, {'false':'女'}],
                align: 'center',
                width: 80,
                render: function(value) {
                    return String(value) == 'true' ? '男' : '女'
                }
            },
            ]
        },
        {
            label: '活动信息',
            columns: [
            {
                name: 'status',
                label: '状态',
                align: 'center',
                type: 'number',
                width: 100,
                render: function(value) {
                    return parseInt(value)
                }
            },
            {
                name: 'created_at',
                label: '创建时间',
//                 hide: true,
                type: 'date',
                pattern: 'yyyy-MM-dd HH:mm:ss',
                render: function(value) {
                	var date = new Date(parseInt(value) * 1000);
                	return date.formatDate('yyyy-MM-dd HH:mm:ss');
                }
            },
            {
                name: 'updated_at',
                label: '更新时间',
                type: 'date',
                pattern: 'yyyy-MM-dd HH:mm:ss',
                render: function(value) {
                	var date = new Date(parseInt(value) * 1000);
                	return date.formatDate('yyyy-MM-dd HH:mm:ss');
                }
            }
            ]
        },
    ],
    editUrl: '<?= Url::toRoute('user/update') ?>',
    delUrl: '<?= Url::toRoute('user/delete') ?>',
    delPK: 'id',
    paging: {pageSize:10, selectPageSize:'20,30,40,50', pageCurrent:1, showPagenum:10},
    editMode: 'dialog',
    showEditbtnscol: false,
    linenumberAll: true,
    fullGrid:true,
    showLinenumber: true,
    showCheckboxcol: true,
    fieldSortable: false,
    columnMenu: false,
    contextMenuB: true,
})
</script>
<div class="bjui-pageContent tableContent">
	<table id="user-datagrid" data-width="100%" data-height="500" class="table table-bordered">
	</table>
</div>