<?php use yii\helpers\Url; ?>
<script type="text/javascript">
//单击事件
function ZtreeClick(event, treeId, treeNode) {
    event.preventDefault()
    if(treeNode.pid == 0){
        return false;
    }
    var $detail = $('#ztree-detail')
    
    if ($detail.attr('tid') == treeNode.tId) return
	if (treeNode.name) $('#j_menu_name').val(treeNode.name)
	if (treeNode.url) {
		$('#j_menu_url').val(treeNode.url)
	} else {
		$('#j_menu_url').val('')
	}
	if (treeNode.tabid) {
		$('#j_menu_tabid').val(treeNode.tabid)
	} else {
		$('#j_menu_tabid').val('')
	}
	if (treeNode.target) {
		$('#j_menu_target').selectpicker('val', treeNode.target)
	} else {
		$('#j_menu_target').selectpicker('val', '')
	}
	$detail.attr('tid', treeNode.tId)
    $detail.show()
}
//保存属性
function M_Ts_Menu() {
	var zTree  = $.fn.zTree.getZTreeObj("core-menu-index-tree")
	var name   = $('#j_menu_name').val()
	var url    = $('#j_menu_url').val()
	var tabid  = $('#j_menu_tabid').val()
	var target = $('#j_menu_target').val()
	
	if ($.trim(name).length == 0) {
		 $(this).alertmsg('error', '节点名称不能为空！');
		return;
	}
	var upNode = zTree.getSelectedNodes()[0]
	
	if (!upNode) {
		 $(this).alertmsg('error', '未选中任何节点！');
	}
	upNode.pid = upNode.getParentNode().id
	upNode.name   = name
	upNode.url    = url
	upNode.tabid  = tabid
	upNode.target = target
// 	zTree.updateNode(upNode);
	$(this).bjuiajax('doAjax', {
		type:'post',
		url:'<?= Url::toRoute(['menu/save','id'=>'']) ?>'+(upNode.id?upNode.id:0),
		data:{
			_csrf:'<?= Yii::$app->request->csrfToken ?>',
		 	pid:upNode.pid,
		 	// 初始化默认分组
		 	group_id:1,
			name:upNode.name,
			url:upNode.url,
			tabid:upNode.tabid,
			target:upNode.target,
		},
		callback:function(json){
    		if(json.statusCode == 200){
    			$(this).bjuiajax('ajaxDone', json);       // 信息提示
    			upNode.id   = json.id;
    			zTree.updateNode(upNode);
    			$('#diyBtn_add_undefined').remove();
    			$('#diyBtn_del_undefined').remove();
    		}else{
    			$(this).bjuiajax('ajaxDone', json)       // 信息提示
    			.navtab('refresh')                // 刷新当前navtab
    		}
    	}
	});
}
//
function M_BeforeNodeDrag(treeId, treeNodes) {
// 	alert(treeId + treeNodes[0].id + treeNodes[0].name);
    return true
}
//监听拖拽事件
function M_BeforeNodeDrop(treeId, treeNodes, targetNode, moveType, isCopy) {
	if(targetNode == null){
		return false;
	}
	if(moveType == 'prev' || moveType == 'next'){
		return false;
	}
	if(treeNodes[0].id == null){
		return false;
	}
	$(this).bjuiajax('doAjax', {type:'get', url:'<?= Url::toRoute('menu/node-drop') ?>', confirmMsg:'你确定要移动节点['+treeNodes[0].name+']到节点['+targetNode.name+']下吗?', data:{pid:targetNode.id, id:treeNodes[0].id, movetype:moveType}, callback:function(json){
		if(json.statusCode == 200){
			$(this).bjuiajax('ajaxDone', json);       // 信息提示
			var zTree  = $.fn.zTree.getZTreeObj("core-menu-index-tree");
			treeNodes[0].pid   = targetNode.id;
			zTree.updateNode(treeNodes[0]);
		}else{
			$(this).bjuiajax('ajaxDone', json)       // 信息提示
			.navtab('refresh')                // 刷新当前navtab
		}
	} });
// 	alert(treeNodes + targetNode.id + moveType +isCopy);
}
//拖拽结束事件
function M_NodeDrop(event, treeId, treeNodes, targetNode, moveType, isCopy) {
// 	alert(0);
//     var $log = $('#ztree-log')
//     $log.val('拖拽结束！\n'+ $log.val())
}
//删除前事件
function M_BeforeRemove(treeId, treeNode) {
// 	alert(treeId + treeNode.id);
//     var $log = $('#ztree-log')
//     $log.val('准备删除：'+ treeNode.name +'！\n'+ $log.val())
    return true
}
//删除结束事件
function M_NodeRemove(event, treeId, treeNode) {
	$(this).bjuiajax('doAjax', {type:'post', url:'<?= Url::toRoute(['menu/delete','id'=>'']) ?>'+treeNode.id, data:{_csrf:'<?= Yii::$app->request->csrfToken ?>'}, callback:function(json){
		if(json.statusCode == 200){
			$(this).bjuiajax('ajaxDone', json);       // 信息提示
			return false;
		}else{
			$(this).bjuiajax('ajaxDone', json)       // 信息提示
			.navtab('refresh')                // 刷新当前navtab
		}
	} });
// 	alert(treeId + treeNode.id);
//     var $log = $('#ztree-log')
//     $log.val('删除成功！\n'+ $log.val())
}
</script>
<style type="text/css">
#core-menu-index-tree li span.button.switch.level0 {
	visibility: hidden;
	width: 1px;
}

#core-menu-index-tree li ul.level0 {
	padding: 0;
	background: none;
}

#core-menu-index-tree #diyBtn_del_1 {
	display: none;
}
</style>
<div class="bjui-pageContent">
    <div class="clearfix">
        <div style="float: left; width: 220px; height: 400px; overflow: auto;">
            <ul id="core-menu-index-tree" class="ztree" data-toggle="ztree" data-options="{
                        expandAll: false,
                        onClick: 'ZtreeClick',
                        addHoverDom :'edit',
                        removeHoverDom: 'edit',
                        editEnable: true,
                        beforeDrag: 'M_BeforeNodeDrag',
                        beforeDrop: 'M_BeforeNodeDrop',
                        onDrop: 'M_NodeDrop',
                        beforeRemove: 'M_BeforeRemove',
                        onRemove: 'M_NodeRemove',
                        maxAddLevel: 999,
<!--                         expandNode: 1, -->
<!--                         checkEnable: true -->
                    }">
                <!--                     echo '<li data-id="1" data-open="true" data-pid="' . $model->pid . '" data-faicon="home" data-faicon-close="home">后台菜单</li>'; -->
                    <?php
                    foreach ($models as $model) {
                        echo '<li data-id="' . $model->id . '" data-pid="' . $model->pid . '" data-url="' . $model->url . '" data-tabid="' . $model->tabid . '" data-faicon="" data-faicon-close="" data-open="' . $model->open . '">' . $model->name . '</li>';
                    }
                    ?>
                </ul>
        </div>
        <div id="ztree-detail" style="display: none; margin-left: 230px; width: 300px; height: 240px;">
            <div class="bs-example" data-content="详细信息">
                <div class="form-group">
                    <label for="j_menu_name" class="control-label x85">节点名称：</label> <input type="text" class="form-control validate[required] required" name="m.name" id="j_menu_name" size="15" placeholder="名称" />
                </div>
                <div class="form-group">
                    <label for="j_menu_url" class="control-label x85">URL：</label> <input type="text" class="form-control" name="m.url" id="j_menu_url" size="15" placeholder="Url" />
                </div>
                <div class="form-group">
                    <label for="j_menu_tabid" class="control-label x85">标签ID：</label> <input type="text" class="form-control" name="m.tabid" id="j_menu_tabid" size="15" placeholder="tabid" />
                </div>
                <div class="form-group">
                    <label for="j_menu_target" class="control-label x85">打开区域：</label> <select class="selectpicker show-tick" id="j_menu_target" data-style="btn-default btn-sel" data-width="auto">
                        <option value=""></option>
                        <option value="navTab">标签工作区</option>
                        <option value="dialog">弹窗工作区</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="j_menu_open" class="control-label x85">节点状态：</label> <select class="selectpicker show-tick" id="j_menu_open" data-style="btn-default btn-sel" data-width="auto">
                        <option value=""></option>
                        <option value="false">折叠</option>
                        <option value="true">展开</option>
                    </select>
                </div>
                <div class="form-group" style="padding-top: 8px; border-top: 1px #DDD solid;">
                    <label class="control-label x85"></label>
                    <button class="btn btn-green" onclick="M_Ts_Menu();">更新节点</button>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <h4>
        <a href="doc/plugin/ztree.html" data-toggle="navtab" data-id="doc-ztree" data-title="zTree">点我查看初始化方法及参数设置</a>
    </h4>
</div>
<div class="bjui-pageFooter">
    <ul>
        <li>
            <button type="button" class="btn-close" data-icon="close">关闭</button>
        </li>
    </ul>
</div>