<?php
namespace backend\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\helpers\Json;
use common\models\Menu;
use common\models\Group;

class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'ajax-login', 'logout', 'timeout', 'error'],
                        'allow' => true
                    ],
                    [
                        'actions' => ['index', 'about', 'layout', 'git'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ]
        ];
    }

    public function actionIndex()
    {
        $this->layout = 'bjui';
        $query = Group::find();
        $models = $query->where(['pid'=>0])->with('groups')->all();
        return $this->render('index', [
            'models' => $models
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionLogin()
    {
        $this->layout = false;
        if (! \Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('/');
        } else {
            return $this->render('login', [
                'model' => $model
            ]);
        }
    }

    public function actionAjaxLogin()
    {
        $this->layout = false;
        $model = new LoginForm();
        $data = [];
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $data['statusCode'] = 200;
            $data['message'] = '登陆成功';
            $data['closeCurrent'] = true;
            return Json::encode($data);
        } else {
            $data['statusCode'] = 300;
            $data['message'] = '登陆失败';
            return Json::encode($data);
        }
    }

    public function actionTimeout()
    {
        return $this->render('timeout');
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        $data = [
            'statusCode' => 200,
            'closeCurrent' => true,
            'message' => '注销成功'
        ];
        return Json::encode($data);
    }

    public function actionLayout()
    {
        return $this->render('layout');
    }
    
    public function actionGit()
    {
        return $this->render('git');
    }
}
