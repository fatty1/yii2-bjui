<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Asset bundle for the Twitter bootstrap css files.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class BjuiAsset extends AssetBundle
{
    
    // 源路径对应的物理目录 /vendor/bower/bjui
    // 应用运行时会直接发布到 @web/assets 下,对应物理目录 /backend/web/assets
    public $sourcePath = '@bower/bjui';
    
    // 指定页面要引入的 css , 也可以后期通过 registerCssFile 引入
    public $css = [
        // bootstrap - css
        'themes/css/bootstrap.css',
        // core - css
        'themes/css/style.css',
        'themes/css/doc.css',
        'themes/blue/core.css',
        // plug - css
        'plugins/kindeditor_4.1.10/themes/default/default.css',
        'plugins/colorpicker/css/bootstrap-colorpicker.min.css',
        'plugins/niceValidator/jquery.validator.css',
        'plugins/bootstrapSelect/bootstrap-select.css',
        'plugins/syntaxhighlighter-2.1.382/styles/shCore.css',
        'plugins/syntaxhighlighter-2.1.382/styles/shThemeEclipse.css',
        // other
        'themes/css/FA/css/font-awesome.min.css',
        'plugins/uploadify/css/uploadify.css'
    ];
    
    // 指定页面要引入的 js , 也可以后期通过 registerJsFile 引入
    public $js = [
        // jquery
        'js/jquery-1.7.2.min.js',
        'js/jquery.cookie.js',
        // BJUI.all 分模块压缩版
        'js/bjui-all.js',
        // 以下是B-JUI的分模块未压缩版，建议开发调试阶段使用下面的版本
        // 'js/bjui-core.js',
        // 'js/bjui-regional.zh-CN.js',
        // 'js/bjui-frag.js',
        // 'js/bjui-extends.js',
        // 'js/bjui-basedrag.js',
        // 'js/bjui-slidebar.js',
        // 'js/bjui-contextmenu.js',
        // 'js/bjui-navtab.js',
        // 'js/bjui-dialog.js',
        // 'js/bjui-taskbar.js',
        // 'js/bjui-ajax.js',
        // 'js/bjui-alertmsg.js',
        // 'js/bjui-pagination.js',
        // 'js/bjui-util.date.js',
        // 'js/bjui-datepicker.js',
        // 'js/bjui-ajaxtab.js',
        // 'js/bjui-datagrid.js',
        // 'js/bjui-tablefixed.js',
        // 'js/bjui-tabledit.js',
        // 'js/bjui-spinner.js',
        // 'js/bjui-lookup.js',
        // 'js/bjui-tags.js',
        // 'js/bjui-upload.js',
        // 'js/bjui-theme.js',
        // 'js/bjui-initui.js',
        // 'js/bjui-plugins.js',
        // plugins
        // swfupload for uploadify && kindeditor
        'plugins/swfupload/swfupload.js',
        // kindeditor
        'plugins/kindeditor_4.1.10/kindeditor-all.min.js',
        'plugins/kindeditor_4.1.10/lang/zh_CN.js',
        // colorpicker
        'plugins/colorpicker/js/bootstrap-colorpicker.min.js',
        // ztree
        'plugins/ztree/jquery.ztree.all-3.5.js',
        // nice validate
        'plugins/niceValidator/jquery.validator.js',
        'plugins/niceValidator/jquery.validator.themes.js',
        
        // bootstrap plugins
        'plugins/bootstrap.min.js',
        'plugins/bootstrapSelect/bootstrap-select.min.js',
        'plugins/bootstrapSelect/defaults-zh_CN.min.js',
        // icheck
        'plugins/icheck/icheck.min.js',
        // dragsort
        'plugins/dragsort/jquery.dragsort-0.5.1.min.js',
        // HighCharts
        // 'plugins/highcharts/highcharts.js',
        // 'plugins/highcharts/highcharts-3d.js',
        // 'plugins/highcharts/themes/gray.js',
        // ECharts
        // 'plugins/echarts/echarts.js',
        // other plugins
        'plugins/other/jquery.autosize.js',
        'plugins/uploadify/scripts/jquery.uploadify.min.js',
        'plugins/download/jquery.fileDownload.js',
        'plugins/syntaxhighlighter-2.1.382/scripts/brush.js'
    ];
}
