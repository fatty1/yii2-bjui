<?php
namespace backend\modules\core\controllers;

use Yii;
use backend\controllers\BjuiController;
use yii\db\Query;

class AssignController extends BjuiController
{

    public function actionIndex()
    {
        $query = new Query();
        $query->select('*')
            ->from('mxq_auth_item')
            ->leftJoin('mxq_auth_item_child', 'mxq_auth_item.name=mxq_auth_item_child.child')
            ->where([
            'mxq_auth_item.type' => 1
        ]);
        $models = $query->all();
        return $this->render('index', [
            'models' => $models
        ]);
    }

    public function actionPermissions($name)
    {
        $role = $this->auth->getRoles();
        $roles = array_keys($role);
        $query = new Query();
        $query->select('*')
            ->from('mxq_auth_item')
            ->leftJoin('mxq_auth_item_child', [
            'and',
            'mxq_auth_item.name=mxq_auth_item_child.child',
            'mxq_auth_item_child.parent not in (\'' . implode("','", $roles) . '\')'
        ])
            ->where([
            'mxq_auth_item.type' => 2
        ]);
        $models = $query->all();
        $hasModel = $this->auth->getPermissionsByRole($name);
        $hasModels = array_keys($hasModel);
        return $this->render('permissions', [
            'models' => $models,
            'hasModels' => $hasModels
        ]);
    }

    public function actionSave($name)
    {
        $parent = $this->auth->getRole($name);
        $action = Yii::$app->request->post('action');
        $level = Yii::$app->request->post('level');
        $ids = Yii::$app->request->post('ids');
        $ids = explode(',', $ids);
        
        if ($level == 0) {
            $child = $this->auth->getPermission($ids[$level]);
            if ($action == 'add') {
                $this->auth->addChild($parent, $child);
            } else {
                $this->auth->removeChild($parent, $child);
            }
            // 下级节点
            foreach ($this->auth->getChildren($ids[$level]) as $key => $value) {
                $child = $this->auth->getPermission($value->name);
                $this->auth->removeChild($parent, $child);
                foreach ($this->auth->getChildren($value->name) as $k => $v) {
                    $child = $this->auth->getPermission($v->name);
                    $this->auth->removeChild($parent, $child);
                }
            }
        } elseif ($level == 1) {
            // 根节点
            $child = $this->auth->getPermission($ids[0]);
            if ($this->auth->hasChild($parent, $child)) {
                foreach ($this->auth->getChildren($ids[0]) as $key => $value) {
                    $child0 = $this->auth->getPermission($value->name);
                    $this->auth->addChild($parent, $child0);
                }
                $this->auth->removeChild($parent, $child);
            }
            // 父节点
            $child = $this->auth->getPermission($ids[$level]);
            if ($action == 'add') {
                $this->auth->addChild($parent, $child);
            } else {
                $this->auth->removeChild($parent, $child);
            }
            // 叶子节点
            foreach ($this->auth->getChildren($ids[$level]) as $key => $value) {
                $child = $this->auth->getPermission($value->name);
                $this->auth->removeChild($parent, $child);
            }
        } elseif ($level == 2) {
            // 根节点
            $child = $this->auth->getPermission($ids[0]);
            if ($this->auth->hasChild($parent, $child)) {
                foreach ($this->auth->getChildren($ids[0]) as $key => $value) {
                    $child0 = $this->auth->getPermission($value->name);
                    $this->auth->addChild($parent, $child0);
                }
                $this->auth->removeChild($parent, $child);
            }
            // 父节点
            $child = $this->auth->getPermission($ids[1]);
            if ($this->auth->hasChild($parent, $child)) {
                foreach ($this->auth->getChildren($ids[1]) as $key => $value) {
                    $child0 = $this->auth->getPermission($value->name);
                    $this->auth->addChild($parent, $child0);
                }
                $this->auth->removeChild($parent, $child);
            }
            // 叶子节点
            $child = $this->auth->getPermission($ids[$level]);
            if ($action == 'add') {
                $this->auth->addChild($parent, $child);
            } else {
                $this->auth->removeChild($parent, $child);
            }
        }
        
        return $this->ok('保存成功');
    }
}