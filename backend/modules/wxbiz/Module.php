<?php

namespace backend\modules\wxbiz;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\wxbiz\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
