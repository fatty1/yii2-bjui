<?php
namespace backend\modules\wxbiz\controllers;

use yii\web\Controller;

class ServiceController extends Controller
{

    // 张成波修改
    public $name;

    public $info_name;

    public $path;

    public $params;

    public $sleep = 5;

    public function install()
    {
        /* 注册服务 */
        $x = win32_create_service(array(
            'service' => $this->name,
            'display' => $this->info_name,
            'path' => $this->path,
            'params' => $this->params
        ));
        /* 启动服务 */
        win32_start_service($this->name);
        if ($x !== true) {
            die('服务创建失败!');
        } else {
            die('服务创建成功!');
        }
    }

    public function uninstall()
    {
        /* 移除服务 */
        $removeService = win32_delete_service($this->name);
        switch ($removeService) {
            case 1060:
                die('服务不存在！');
                break;
            case 1072:
                die('服务不能被正常移除! ');
                break;
            case 0:
                die('服务已被成功移除！');
                break;
            default:
                die();
                break;
        }
    }

    public function restart()
    {
        /* 重启服务 */
        $svcStatus = win32_query_service_status($this->name);
        if ($svcStatus == 1060) {
            echo "服务[" . $this->name . "]未被安装，请先安装";
        } else {
            if ($svcStatus['CurrentState'] == 1) {
                $s = win32_start_service($this->name);
                if ($s != 0) {
                    echo "服务无法被启动，请重试！ ";
                } else {
                    echo "服务已启动! ";
                }
            } else {
                $s = win32_stop_service($this->name);
                if ($s != 0) {
                    echo " 服务正在执行，请重试！ ";
                } else {
                    $s = win32_start_service($this->name);
                    if ($s != 0) {
                        echo "服务无法被启动，请重试！ ";
                    } else {
                        echo "服务已启动! ";
                    }
                }
            }
        }
    }

    public function start()
    {
        $s = win32_start_service(_SERVICENAME);
        if ($s != 0) {
            echo " 服务正在运行中！ ";
        } else {
            echo " 服务已启动! ";
        }
    }

    public function stop()
    {
        $s = win32_stop_service(_SERVICENAME);
        if ($s != 0) {
            echo " 服务未启动！ ";
        } else {
            echo " 服务已停止！ ";
        }
    }
}