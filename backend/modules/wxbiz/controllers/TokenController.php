<?php
namespace backend\modules\wxbiz\controllers;

use Yii;
use backend\controllers\BjuiController;
use lib\wxbiz\WechatCache;
use lib\wxbiz\Menu;

class TokenController extends BjuiController
{

    public function actionIndex($id)
    {
        $access_token = WechatCache::getAccessToken($id);
        echo $access_token;
    }

    public function actionMenu($id)
    {
        $cache = Yii::$app->cache;
//         $cache->flush();
        echo $cache->get('wxbiz.token.' . $id);
        echo '<br>';
        echo $cache->get('wxbiz.access_token.' . $id);
        echo '<br>';
        echo $cache->get('wxbiz.agentid.' . $id);
        echo '<br>';
//         var_dump(Menu::delMenu($id));
        $menu = Menu::getMenu($id);
//         Menu::setMenu($id, $menuList);
        var_dump($menu);
    }
}