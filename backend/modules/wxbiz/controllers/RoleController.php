<?php

namespace backend\modules\wxbiz\controllers;

use Yii;
use common\models\WxbizRole;
use backend\modules\wxbiz\models\WxbizRoleSearch;
use yii\filters\VerbFilter;
use backend\controllers\BjuiController;
use common\models\WxbizCorp;

/**
 * WxbizRoleController implements the CRUD actions for WxbizRole model.
 */
class RoleController extends BjuiController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all WxbizRole models.
     * @return mixed
     */
    public function actionIndex()
    {
        $models = WxbizCorp::find()->all();
        return $this->render('index', [
            'models' => $models,
        ]);
    }

    /**
     * Lists all WxbizRole models.
     * @return mixed
     */
    public function actionSearch()
    {
        $searchModel = new WxbizRoleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('search', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single WxbizRole model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new WxbizRole model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new WxbizRole();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->data['divid'] = 'wxbiz-role-search';
            $this->data['closeCurrent'] = true;
            return $this->ok('增加成功');
        } else {
            return $this->error(array_values($model->getFirstErrors()));
        }
    }

    /**
     * Updates an existing WxbizRole model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->data['divid'] = 'wxbiz-role-search';
            $this->data['closeCurrent'] = true;
            $this->RoleFlush($id);
            return $this->ok('更新成功');
        } else {
            return $this->error(array_values($model->getFirstErrors()));
        }
    }

    /**
     * 批量更新企业号下所有应用缓存
     */
    public function RoleFlush($id){
        $model = $this->findModel($id);
        $cache = Yii::$app->cache;
        foreach ($model->agents as $agent){
            $cache->set('wxbiz.secret.'.$agent->id, $model->secret, 600);
        }
    }

    /**
     * Deletes an existing WxbizRole model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        $this->data['divid'] = 'wxbiz-role-search';
        return $this->ok('删除成功');
    }

    // 批量删除
    public function actionBatchDelete($delids)
    {
        $delids = explode(',', $delids);
        $model = WxbizRole::deleteAll([
            'id' => $delids
        ]);
        $this->data['divid'] = 'wxbiz-role-search';
        return $this->ok('删除成功');
    }

    /**
     * Finds the WxbizRole model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return WxbizRole the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = WxbizRole::findOne($id)) !== null) {
            return $model;
        } else {
            return new WxbizRole();
        }
    }
}
