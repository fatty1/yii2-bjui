<?php
namespace backend\modules\wxbiz\controllers;

use Yii;
use backend\controllers\BjuiController;
use lib\wxbiz\Message;

class MessageController extends BjuiController
{

    public function actionIndex($id)
    {

    }

    public function actionSend($id)
    {
        $data = [
            "touser" => "zhangchengbo|zhangsan",
            "toparty" => "2|3",
            "totag" => "1",
            "msgtype" => "text",
            "agentid" => "2",
            "text" => [
                "content" => "测试信息" . time()
            ],
            "safe" => "0"
        ];
        $message = Message::Send($id, $data);
        return var_dump($message);
    }
}
