<?php

namespace backend\modules\wxbiz\controllers;

use Yii;
use common\models\WxbizCorp;
use backend\modules\wxbiz\models\WxbizCorpSearch;
use yii\filters\VerbFilter;
use backend\controllers\BjuiController;
use common\models\WxbizAgent;

/**
 * WxbizCorpController implements the CRUD actions for WxbizCorp model.
 */
class CorpController extends BjuiController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all WxbizCorp models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new WxbizCorpSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single WxbizCorp model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new WxbizCorp model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new WxbizCorp();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->data['tabid'] = 'wxbiz-corp';
            $this->data['closeCurrent'] = true;
            return $this->ok('增加成功');
        } else {
            return $this->error(array_values($model->getFirstErrors()));
        }
    }

    /**
     * Updates an existing WxbizCorp model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->data['tabid'] = 'wxbiz-corp';
            $this->data['closeCurrent'] = true;
            $this->CorpFlush($id);
            return $this->ok('更新成功');
        } else {
            return $this->error(array_values($model->getFirstErrors()));
        }
    }
    
    /**
     * 批量更新企业号下所有应用缓存
     */
    public function CorpFlush($id){
        $model = $this->findModel($id);
        $cache = Yii::$app->cache;
        foreach ($model->agents as $agent){
            $cache->set('wxbiz.corpId.'.$agent->id, $model->corpId, 600);
        }
    }

    /**
     * Deletes an existing WxbizCorp model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->ok('删除成功');
    }

    // 批量删除
    public function actionBatchDelete($delids)
    {
        $delids = explode(',', $delids);
        $model = WxbizCorp::deleteAll([
            'id' => $delids
        ]);
        return $this->ok('删除成功');
    }

    /**
     * Finds the WxbizCorp model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return WxbizCorp the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = WxbizCorp::findOne($id)) !== null) {
            return $model;
        } else {
            return new WxbizCorp();
        }
    }
}
