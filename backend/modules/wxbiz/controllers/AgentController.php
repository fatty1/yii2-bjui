<?php

namespace backend\modules\wxbiz\controllers;

use Yii;
use common\models\WxbizAgent;
use backend\modules\wxbiz\models\WxbizAgentSearch;
use yii\filters\VerbFilter;
use backend\controllers\BjuiController;
use common\models\WxbizCorp;

/**
 * WxbizAgentController implements the CRUD actions for WxbizAgent model.
 */
class AgentController extends BjuiController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all WxbizAgent models.
     * @return mixed
     */
    public function actionIndex()
    {
        $models = WxbizCorp::find()->with('roles')->all();
        return $this->render('index', [
            'models' => $models,
        ]);
    }

    /**
     * Lists all WxbizAgent models.
     * @return mixed
     */
    public function actionSearch()
    {
        $searchModel = new WxbizAgentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('search', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single WxbizAgent model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new WxbizAgent model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new WxbizAgent();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->data['divid'] = 'wxbiz-agent-search';
            $this->data['closeCurrent'] = true;
            return $this->ok('增加成功');
        } else {
            return $this->error(array_values($model->getFirstErrors()));
        }
    }

    /**
     * Updates an existing WxbizAgent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $cache = Yii::$app->cache;
            $cache->set('wxbiz.token.'.$model->id, $model->token, 600);
            $cache->set('wxbiz.encodingAesKey.'.$model->id, $model->encodingAesKey, 600);
            $cache->set('wxbiz.agentid.'.$model->id, $model->agentid, 600);
            $this->data['divid'] = 'wxbiz-agent-search';
            $this->data['closeCurrent'] = true;
            return $this->ok('更新成功');
        } else {
            return $this->error(array_values($model->getFirstErrors()));
        }
    }

    /**
     * Deletes an existing WxbizAgent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        $this->data['divid'] = 'wxbiz-agent-search';
        return $this->ok('删除成功');
    }

    // 批量删除
    public function actionBatchDelete($delids)
    {
        $delids = explode(',', $delids);
        $model = WxbizAgent::deleteAll([
            'id' => $delids
        ]);
        $this->data['divid'] = 'wxbiz-agent-search';
        return $this->ok('删除成功');
    }

    /**
     * Finds the WxbizAgent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return WxbizAgent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = WxbizAgent::findOne($id)) !== null) {
            return $model;
        } else {
            return new WxbizAgent();
        }
    }
}
