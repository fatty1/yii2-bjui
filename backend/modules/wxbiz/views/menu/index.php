<?php use yii\helpers\Url; ?>
<script type="text/javascript">
function do_open_layout(event, treeId, treeNode) {
	if(treeNode.menutype != 'dropdown'){
		if(treeNode.pid>0){
            $(event.target).bjuiajax('doLoad', {url:'<?= Url::toRoute('ztree')?>&agent_id='+treeNode.agent_id, target:'#wxbiz-menu-ztree'});
		}
        event.preventDefault();
	}
}
</script>
<div class="bjui-pageContent">
    <div style="float: left; width: 200px; height: 99.9%; overflow: auto;">
        <ul id="wxbiz-menu-index-ztree" class="ztree" data-toggle="ztree" data-options="{
            expandAll: true,
            onClick: 'do_open_layout',
            maxAddLevel: 1
        }">
        <?php
        foreach ($models as $model) {
            echo '<li data-id="' . $model->id . '" data-pid="0">' . $model->name . '</li>';
            foreach ($model->agents as $agent) {
                echo '<li data-id="' . $model->id . '-' . $agent->id . '" data-pid="' . $model->id . '" data-agent_id="' . $agent->id . '">' . $agent->name . '</li>';
            }
        }
        ?>
        </ul>
    </div>
    <div style="margin-left: 210px; height: 99.9%; overflow: hidden;">
        <fieldset style="height: 100%;">
            <legend>自定义菜单管理</legend>
            <div id="wxbiz-menu-ztree" style="height: 100%; overflow: hidden;"></div>
        </fieldset>
    </div>
</div>