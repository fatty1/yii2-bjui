<?php use yii\helpers\Url; ?>
<script type="text/javascript">
function myCallback(json) {
	$(this).bjuiajax('ajaxDone', json);
    if(json.statusCode == 200){
    	$(this).dialog('closeCurrent');
		// 增加修改 Ztree 节点
    	var treeId = "wxbiz-menu-ztree-ztree";
		var zTree = $.fn.zTree.getZTreeObj(treeId);
		// 因为节点的 name 值是变化的,所以通过 tId 判断是否修改节点
		var treeNode = zTree.getNodeByTId($('#tId').val());
		if(treeNode){
			// 修改节点
			removeHoverDom(treeId, treeNode);
			treeNode.id = json.id;
			treeNode.name = $('#name').val();
			zTree.updateNode(treeNode);
		}else{
			// 增加节点
			var parentNode = zTree.getNodeByParam("id", $('#pid').val(), null);
			var newNode = {id:json.id, name:$('#name').val()};
			if(parentNode){
			    zTree.addNodes(parentNode, newNode);
			}else{
			    zTree.addNodes(null, newNode);
			}
		}
    }
}

function addHoverDom(treeId, treeNode) {
	var aObj = $("#" + treeNode.tId + "_a");
	if ($("#wxbizMenuZtree_add_"+treeNode.id).length>0) return;
	if ($("#wxbizMenuZtree_edit_"+treeNode.id).length>0) return;
	if ($("#wxbizMenuZtree_del_"+treeNode.id).length>0) return;
	var editStr = "<i>";
	if(treeNode.level<1){
		 editStr =  editStr + "<span title='添加' id='wxbizMenuZtree_add_" +treeNode.id+ "' class='tree_add' > </span>"
	}
	editStr =  editStr + "<span title='修改' id='wxbizMenuZtree_edit_" +treeNode.id+ "' class='button edit' > </span>"
	if(!treeNode.isParent){
		editStr =  editStr + "<span title='删除' id='wxbizMenuZtree_del_" +treeNode.id+ "' class='tree_del' > </span>";
	}
	aObj.append(editStr + "</i>");
	$("#wxbizMenuZtree_add_"+treeNode.id).bind("click", function(){
		if(treeNode.children){
			if(treeNode.children.length>=5){
				$(this).alertmsg('warn', '二级菜单最多为5个');
				return false;
			}
		}
		$(this).dialog({id:'wxbiz-menu-view', url:'<?= Url::toRoute(['view']) ?>&tId='+treeNode.tId+'&agent_id=<?= Yii::$app->request->get('agent_id') ?>&pid='+treeNode.id+'&id=', title:'增加节点', width:'650', height:'300'});
	});
	$("#wxbizMenuZtree_edit_"+treeNode.id).bind("click", function(){
		$(this).dialog({id:'wxbiz-menu-view', url:'<?= Url::toRoute(['view']) ?>&tId='+treeNode.tId+'&id='+treeNode.id, title:'修改节点', width:'650', height:'300'});
	});
	$("#wxbizMenuZtree_del_"+treeNode.id).bind("click", function(){
        $(this).alertmsg('confirm', '确定要删除 '+treeNode.name+' 节点吗？', {okCall:function(){
            $(this).bjuiajax('doAjax', {type:'post', url:'<?= Url::toRoute(['delete']) ?>&id='+treeNode.id, data:{_csrf:'<?= Yii::$app->request->csrfToken ?>'}, callback:function(json){
            	$(this).bjuiajax('ajaxDone', json);
            	if(json.statusCode == 200){
         		   // 删除 Ztree 节点
            		var zTree = $.fn.zTree.getZTreeObj(treeId);
            		zTree.removeNode(treeNode);
            	}
            }});
        }});
	});
};
function removeHoverDom(treeId, treeNode) {
// 	alert(treeNode.id);
	$("#wxbizMenuZtree_add_"+treeNode.id).parent().remove();
	$("#wxbizMenuZtree_add_"+treeNode.id).unbind().remove();
	$("#wxbizMenuZtree_edit_"+treeNode.id).unbind().remove();
	$("#wxbizMenuZtree_del_" +treeNode.id).unbind().remove();
};
function addMainNode() {
	var zTree = $.fn.zTree.getZTreeObj('wxbiz-menu-ztree-ztree');
	if(zTree.getNodes().length >= 3){
		$(this).alertmsg('warn', '一级菜单最多为3个');
	}else{
		$(this).dialog({
			id:'wxbiz-menu-view',
			url:'<?= Url::toRoute(['view', 'agent_id'=>Yii::$app->request->get('agent_id'), 'pid'=>Yii::$app->request->get('id',0), 'id'=>'']) ?>',
			title:'增加主节点',
			width:'650',
			height:'300',
			reloadWarn:'本页已有打开的内容，确定将刷新本页内容，是否继续？'
		});
	}
};
</script>
<div class="bjui-pageHeader">
    <button type="button" class="btn-green" onclick="addMainNode();">增加主节点</button>
    <a href="<?= Url::toRoute(['publish', 'agent_id' => Yii::$app->request->get('agent_id')]) ?>" class="btn btn-red" data-toggle="doajax" data-data="_csrf=<?= Yii::$app->request->csrfToken ?>" data-confirm-msg="确定要发布该自定义菜单？">发布</a>
</div>
<div class="bjui-pageContent tableContent">
    <ul id="wxbiz-menu-ztree-ztree" class="ztree" data-toggle="ztree" data-options="{
            expandAll: true,
            addHoverDom: 'addHoverDom',
            removeHoverDom: 'removeHoverDom',
        }">
        <?php
        foreach ($models as $model) {
            echo '<li data-id="' . $model->id . '" data-pid="' . $model->pid . '">' . $model->name . '</li>';
        }
        ?>
    </ul>
</div>