<?php
use yii\helpers\Url;
?>
<script>

</script>
<div class="bjui-pageContent">
    <form action="<?= Url::toRoute([$model->id?'update':'create','id'=>$model->id]) ?>" id="wxbiz_menu_view_form" data-toggle="validate" data-alertmsg="false" data-callback="myCallback">
        <input name="WxbizMenu[agent_id]" type="hidden" id="agent_id" value="<?= Yii::$app->request->get('agent_id',$model->agent_id); ?>">
        <input name="WxbizMenu[pid]" type="hidden" id="pid" value="<?= Yii::$app->request->get('pid',$model->pid); ?>">
        <input type="hidden" id="tId" value="<?= $model->id?Yii::$app->request->get('tId'):'' ?>">
        <input name="_csrf" type="hidden" id="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                    <td>
                        <label for="type" class="control-label x120">按钮类型：</label>
                        <select class="selectpicker show-tick" name="WxbizMenu[type]" id="type" data-style="btn-default btn-sel" data-width="auto">
                        <?php
                        foreach (Yii::$app->params['wxbizMenuType'] as $key => $value) {
                            echo '<option value="' . $key . '"' . ($model->type === $key ? ' selected="selected"' : '') . '>' . $value . '</option>';
                        }
                        ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="username" class="control-label x120">菜单标题：</label>
                        <input type="text" name="WxbizMenu[name]" id="name" value="<?= $model->name; ?>" data-rule="required;length[1~<?= Yii::$app->request->get('pid',$model->pid)==0?5:8; ?>]" size="20">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="mobile" class="control-label x120">菜单KEY值：</label>
                        <input type="text" name="WxbizMenu[key]" id="key" value="<?= $model->key; ?>" data-rule="" size="30">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="username" class="control-label x120">网页链接：</label>
                        <input type="text" name="WxbizMenu[url]" id="url" value="<?= $model->url; ?>" data-rule="url" size="30">
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="msg-box" id="msgHolder"></span>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
        <li>
            <button type="button" class="btn-close" data-icon="close">取消</button>
        </li>
        <li>
            <button type="submit" class="btn-default" data-icon="save">保存</button>
        </li>
    </ul>
</div>
