<?php
use yii\helpers\Url;
use common\models\WxbizRole;
?>
<div class="bjui-pageContent">
    <form action="<?= Url::toRoute([$model->id?'update':'create','id'=>$model->id]) ?>" id="user_form" data-toggle="validate" data-alertmsg="false">
        <input name="_csrf" type="hidden" id="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
        <input name="WxbizAgent[corp_id]" type="hidden" id="corp_id" value="<?= Yii::$app->request->get('corp_id', $model->corp_id); ?>">
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                    <td>
                        <label for="username" class="control-label x120">应用名称：</label>
                        <input type="text" name="WxbizAgent[name]" id="name" value="<?= $model->name; ?>" data-rule="required" size="20">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="username" class="control-label x120">应用编号：</label>
                        <input type="text" name="WxbizAgent[agentid]" id="agentid" value="<?= $model->agentid; ?>" data-toggle="spinner" data-max="30" data-rule="required; integer; range[0~30];" size="20">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="role_id" class="control-label x120">应用管理组：</label>
                        <select name="WxbizAgent[role_id]" id="role_id" data-toggle="selectpicker" data-rule="required">
                        <?php
                        // 列出所有管理组
                        $roles = WxbizRole::findAll([
                            'corp_id' => Yii::$app->request->get('corp_id', $model->corp_id)
                        ]);
                        foreach ($roles as $role) {
                            echo '<option value="' . $role->id . '"' . (Yii::$app->request->get('role_id', $model->role_id) == $role->id ? ' selected="selected"' : '') . '>' . $role->rolename . '</option>';
                        }
                        ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="mobile" class="control-label x120">令牌：</label>
                        <input type="text" name="WxbizAgent[token]" id="token" value="<?= $model->token; ?>" data-rule="required" size="30">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="username" class="control-label x120">AES密钥：</label>
                        <input type="text" name="WxbizAgent[encodingAesKey]" id="encodingAesKey" value="<?= $model->encodingAesKey; ?>" data-rule="required" size="45">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="status" class="control-label x120">状态：</label>
                        <select name="WxbizAgent[status]" id="status" data-toggle="selectpicker" data-rule="required">
                        <?php
                        foreach (Yii::$app->params['wxbizStatus'] as $key => $value) {
                            echo '<option value="' . $key . '"' . ($model->status === $key ? ' selected="selected"' : '') . '>' . $value . '</option>';
                        }
                        ?>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
        <li>
            <button type="button" class="btn-close" data-icon="close">取消</button>
        </li>
        <li>
            <button type="submit" class="btn-default" data-icon="save">保存</button>
        </li>
    </ul>
</div>
