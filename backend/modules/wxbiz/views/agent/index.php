<?php use yii\helpers\Url; ?>
<script type="text/javascript">
function do_open_layout(event, treeId, treeNode) {
	if(treeNode.menutype != 'dropdown'){
		if(treeNode.pid>0){
            $(event.target).bjuiajax('doLoad', {url:'<?= Url::toRoute('search')?>&corp_id='+treeNode.pid+'&role_id='+treeNode.role_id, target:'#wxbiz-agent-search'});
		}else{
			$(event.target).bjuiajax('doLoad', {url:'<?= Url::toRoute('search')?>&corp_id='+treeNode.id, target:'#wxbiz-agent-search'});
		}
        event.preventDefault();
	}
}
</script>
<div class="bjui-pageContent">
    <div style="float: left; width: 150px; height: 99.9%; overflow: auto;">
        <ul id="wxbiz-agent-index-ztree" class="ztree" data-toggle="ztree" data-options="{
            expandAll: true,
            onClick: 'do_open_layout',
            maxAddLevel: 1
        }">
        <?php
        foreach ($models as $model) {
            echo '<li data-id="' . $model->id . '" data-pid="0">' . $model->name . '</li>';
            foreach($model->roles as $role){
                echo '<li data-id="' . $model->id .'-'. $role->id . '" data-pid="' . $model->id . '" data-role_id="' . $role->id . '">' . $role->rolename . '</li>';
            }
        }
        ?>
        </ul>
    </div>
    <div style="margin-left:160px; height:99.9%; overflow:hidden;">
            <fieldset style="height:100%;">
                <legend>应用管理</legend>
                <div id="wxbiz-agent-search" style="height:100%; overflow:hidden;">
                    
                </div>
            </fieldset>
    </div>
</div>