<?php
use yii\helpers\Url;
?>
<div class="bjui-pageContent">
    <form action="<?= Url::toRoute([$model->id?'update':'create','id'=>$model->id]) ?>" id="user_form" data-toggle="validate" data-alertmsg="false">
        <input name="_csrf" type="hidden" id="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                    <td><label for="username" class="control-label x120">企业名称：</label> <input type="text" name="WxbizCorp[name]" id="name" value="<?= $model->name; ?>" data-rule="required" size="20"></td>
                </tr>
                <tr>
                    <td><label for="mobile" class="control-label x120">企业号标识：</label> <input type="text" name="WxbizCorp[corpId]" id="corpId" value="<?= $model->corpId; ?>" data-rule="required" size="20"></td>
                </tr>
                <tr>
                    <td><label for="status" class="control-label x120">状态：</label> <select name="WxbizCorp[status]" id="status" data-toggle="selectpicker" data-rule="required">
                            <?php
                            foreach (Yii::$app->params['wxbizStatus'] as $key => $value) {
                                echo '<option value="' . $key . '"' . ($model->status === $key ? ' selected="selected"' : '') . '>' . $value . '</option>';
                            }
                            ?>
                        </select></td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
        <li>
            <button type="button" class="btn-close" data-icon="close">取消</button>
        </li>
        <li>
            <button type="submit" class="btn-default" data-icon="save">保存</button>
        </li>
    </ul>
</div>
