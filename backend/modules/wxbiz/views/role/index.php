<?php use yii\helpers\Url; ?>
<script type="text/javascript">
function do_open_layout(event, treeId, treeNode) {
	if(treeNode.menutype != 'dropdown'){
        $(event.target).bjuiajax('doLoad', {url:'<?= Url::toRoute('search')?>&corp_id='+treeNode.id, target:'#wxbiz-role-search'})
        event.preventDefault()
	}
}
</script>
<div class="bjui-pageContent">
    <div style="float: left; width: 150px; height: 99.9%; overflow: auto;">
        <ul id="wxbiz-role-index-ztree" class="ztree" data-toggle="ztree" data-options="{
            expandAll: true,
            onClick: 'do_open_layout',
            maxAddLevel: 1
        }">
        <?php
        foreach ($models as $model) {
            echo '<li data-id="' . $model->id . '">' . $model->name . '</li>';
        }
        ?>
        </ul>
    </div>
    <div style="margin-left: 160px; height: 99.9%; overflow: hidden;">
        <fieldset style="height: 100%;">
            <legend>管理组管理</legend>
            <div id="wxbiz-role-search" style="height: 100%; overflow: hidden;"></div>
        </fieldset>
    </div>
</div>