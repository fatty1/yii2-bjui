<?php
use yii\helpers\Url;
use yii\data\Pagination;
$pages = new Pagination([
    'pageParam' => 'pageCurrent',
    'pageSizeParam' => 'pageSize',
    'totalCount' => $dataProvider->count(),
    'defaultPageSize' => 10
]);

$models = $dataProvider->offset($pages->offset)
    ->limit($pages->limit)
    ->all();
?>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="<?= Url::toRoute(['search', 'corp_id'=>Yii::$app->request->get('corp_id')]) ?>" method="get">
        <input type="hidden" name="pageSize" value="10">
        <input type="hidden" name="pageCurrent" value="1">
        <input type="hidden" name="orderField" value="created_at">
        <input type="hidden" name="orderDirection" value="desc">
        <div class="bjui-searchBar">
            <label>管理组名称：</label>
            <input type="text" value="<?= Yii::$app->request->get('rolename') ?>" name="rolename" class="form-control" size="15">
            &nbsp;
            <button type="submit" class="btn-default" data-icon="search">查询</button>
            &nbsp;
            <a class="btn btn-orange" href="javascript:;" data-toggle="reloadsearch" data-clear-query="true" data-icon="undo">清空查询</a>
            &nbsp;
            <a href="<?= Url::toRoute(['view', 'corp_id'=>Yii::$app->request->get('corp_id'), 'id'=>0]) ?>" class="btn btn-green" data-toggle="dialog" data-id="wxbiz-role-view" data-title="增加管理组" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续？" data-width="800" data-height="200">增加管理组</a>
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn-default dropdown-toggle" data-toggle="dropdown" data-icon="copy">
                        复选框-批量操作
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu right" role="menu">
                        <li class="divider"></li>
                        <li>
                            <a href="<?= Url::toRoute('batch-delete'); ?>" data-toggle="doajaxchecked" data-confirm-msg="确定要删除选中项吗？" data-idname="delids" data-group="ids" data-data="_csrf=<?= Yii::$app->request->csrfToken ?>">删除选中</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="bjui-pageContent tableContent">
    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <tr>
                <th align="center" data-order-field="id" width="50px">序号</th>
                <th align="center" width="100">管理组名称</th>
                <th align="center">管理组凭证密钥</th>
                <th align="center" width="110">更新时间</th>
                <th align="center" width="26">
                    <input type="checkbox" class="checkboxCtrl" data-group="ids" data-toggle="icheck">
                </th>
                <th align="center" width="100">操作</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($models as $model) {?>
            <tr data-id="<?= $model->id ?>">
                <td><?= $model->id ?></td>
                <td><?= $model->rolename ?></td>
                <td><?= $model->secret ?></td>
                <td><?= date('Y-m-d H:i:s',$model->updated_at) ?></td>
                <td align="center">
                    <input type="checkbox" name="ids" data-toggle="icheck" value="<?= $model->id ?>">
                </td>
                <td>
                    <a href="<?= Url::toRoute(['view', 'id' => $model->id]) ?>" class="btn btn-green" data-toggle="dialog" data-id="wxbiz-role-view" data-title="编辑管理组-<?= $model->rolename ?>" data-reload-warn="本页已有打开的内容，确定将刷新本页内容，是否继续？" data-width="800" data-height="200">编辑</a>
                    <a href="<?= Url::toRoute(['delete', 'id' => $model->id]) ?>" class="btn btn-red" data-toggle="doajax" data-data="_csrf=<?= Yii::$app->request->csrfToken ?>" data-confirm-msg="确定要删除该行信息吗？">删</a>
                </td>
            </tr>
            <?php }?>
    </tbody>
    </table>
</div>
<div class="bjui-pageFooter">
    <div class="pages">
        <span>每页&nbsp;</span>
        <div class="selectPagesize">
            <select data-toggle="selectpicker" data-toggle-change="changepagesize">
                <option value="10">10</option>
                <option value="20">20</option>
                <option value="50">50</option>
                <option value="100">100</option>
            </select>
        </div>
        <span>&nbsp;条，共 <?= $pages->totalCount ?> 条</span>
    </div>
    <div class="pagination-box" data-toggle="pagination" data-total="<?= $pages->totalCount ?>" data-page-size="<?= $pages->pageSize ?>" data-page-current="1"></div>
</div>
