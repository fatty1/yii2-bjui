<?php
use yii\helpers\Url;
?>
<div class="bjui-pageContent">
    <form action="<?= Url::toRoute([$model->id?'update':'create','id'=>$model->id]) ?>" id="user_form" data-toggle="validate" data-alertmsg="false">
        <input name="_csrf" type="hidden" id="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
        <input name="WxbizRole[corp_id]" type="hidden" id="corp_id" value="<?= Yii::$app->request->get('corp_id',$model->corp_id); ?>">
        <table class="table table-condensed table-hover" width="100%">
            <tbody>
                <tr>
                    <td>
                        <label for="username" class="control-label x120">管理组名称：</label>
                        <input type="text" name="WxbizRole[rolename]" id="rolename" value="<?= $model->rolename; ?>" data-rule="required" size="20">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="mobile" class="control-label x120">管理组凭证密钥：</label>
                        <input type="text" name="WxbizRole[secret]" id="secret" value="<?= $model->secret; ?>" data-rule="required" size="60">
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<div class="bjui-pageFooter">
    <ul>
        <li>
            <button type="button" class="btn-close" data-icon="close">取消</button>
        </li>
        <li>
            <button type="submit" class="btn-default" data-icon="save">保存</button>
        </li>
    </ul>
</div>
