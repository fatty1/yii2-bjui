<?php

namespace backend\modules\wxbiz\models;

use Yii;
use yii\base\Model;
use common\models\WxbizAgent;

/**
 * WxbizAgentSearch represents the model behind the search form about `common\models\WxbizAgent`.
 */
class WxbizAgentSearch extends WxbizAgent
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'corp_id', 'role_id', 'agentid', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'token', 'encodingAesKey'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WxbizAgent::find();

        $this->attributes = $params;

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $query;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'corp_id' => $this->corp_id,
            'role_id' => $this->role_id,
            'agentid' => $this->agentid,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'token', $this->token])
            ->andFilterWhere(['like', 'encodingAesKey', $this->encodingAesKey]);

        return $query;
    }
}
