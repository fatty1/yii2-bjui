<?php

namespace backend\modules\wxbiz\models;

use Yii;
use yii\base\Model;
use common\models\WxbizRole;

/**
 * WxbizRoleSearch represents the model behind the search form about `common\models\WxbizRole`.
 */
class WxbizRoleSearch extends WxbizRole
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'corp_id', 'created_at', 'updated_at'], 'integer'],
            [['rolename', 'secret'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WxbizRole::find();

        $this->attributes = $params;

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $query;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'corp_id' => $this->corp_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'rolename', $this->rolename])
            ->andFilterWhere(['like', 'secret', $this->secret]);

        return $query;
    }
}
