<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=mxq_yj',
            'username' => 'yj',
            'password' => 'yj',
            'charset' => 'utf8',
            'tablePrefix' => 'mxq_',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
        ],
    ],
];
