Yii 2 整合 B-JUI
===============================
需要看B-JUI完整演示的请移步到 [B-JUI官网](http://b-jui.com)

开发进度
-------------------
    2015-08-15 系统管理->系统初始化->菜单管理
    2015-08-14 系统管理->系统初始化->访问控制

演示地址
-------------------

```
前台http://www.weixin.hnqyw.com
后台http://admin.weixin.hnqyw.com
接口http://api.weixin.hnqyw.com
测试帐号sysadmin
测试密码admin
```

![B-JUI截图](http://git.oschina.net/uploads/images/2015/0628/183855_29abff87_97644.png "datagrid演示")