<?php
namespace api\controllers;

use Yii;
use yii\web\Controller;
use common\models\User;
// use yii\helpers\VarDumper;

/**
 * Site controller
 */
class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actionIndex()
    {
        $model = User::find();
        $user = $model->where([
//             'id' => 1,
            'status' => 10,
        ])
            ->asArray()
            ->all();
        if (version_compare('1.2.3', '6.0.0') >= 0) {
            echo 'I am at least PHP version 6.0.0, my version: ' . PHP_VERSION . "\n";
        }
        echo version_compare('6.02.3', '6.2.3','>=');
        exit;
//         VarDumper::dump($user,10,true);
//         exit();
        return $this->render('index');
    }
}
