<?php
namespace api\controllers;

use yii\rest\Controller;
use yii\filters\Cors;

class RestController extends Controller
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['Cors'] = [
            'class' => Cors::className(),
            'cors' => [
                'Origin' => [
                    'http://www.weixin.hnqyw.com'
                ],
                'Access-Control-Request-Method' => [
                    'POST',
                    'DELETE',
                    'PUT',
                    'GET',
                    'OPTIONS'
                ],
                'Access-Control-Request-Headers' => [
                    '*'
                ]
            ]
        ];
        return $behaviors;
    }
}