<?php
namespace api\controllers;

use yii\rest\ActiveController;
use yii\filters\Cors;

class RestActiveController extends ActiveController
{

    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items'
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['Cors'] = [
            'class' => Cors::className(),
            'cors' => [
                'Origin' => [
                    'http://www.weixin.hnqyw.com'
                ],
                'Access-Control-Request-Method' => [
                    'POST',
                    'DELETE',
                    'PUT',
                    'GET',
                    'OPTIONS'
                ],
                'Access-Control-Request-Headers' => [
                    '*'
                ]
            ]
        ];
        return $behaviors;
    }
}