<?php
namespace api\modules\v1\controllers;

use yii;
use api\modules\v1\models\SignupForm;
use api\controllers\RestActiveController;

class UserController extends RestActiveController
{

    public $modelClass = 'api\modules\v1\models\User';

    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items'
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        // $behaviors['authenticator'] = [
        // 'class' => QueryParamAuth::className()
        // ];
        // 强制输出JSON格式
        // unset($behaviors['contentNegotiator']['formats']['application/xml']);
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        // $actions['create'] = [$this,'actionCreate'];
        return $actions;
    }

    public function actionCreate()
    {
        die('err');
        $model = new SignupForm();
        $model->attributes = Yii::$app->request->post();
        if ($model->validate()) {
            $user = $model->signup();
            if ($user) {
                // 注册成功,自动登陆
                unset($user['password_hash'], $user['auth_key']);
                return $user;
            }
        }
        return $model;
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        // 检查用户能否访问 $action 和 $model
        // 访问被拒绝应抛出ForbiddenHttpException
        // var_dump($params);exit;
    }
}