<?php
namespace api\modules\v1\controllers;

use yii;
use api\controllers\RestController;
use api\modules\v1\models\ForgetForm;

class ForgetController extends RestController
{

    public function actionOption()
    {}

    public function actionCreate()
    {
        $model = new ForgetForm();
        $model->attributes = yii::$app->request->post();
        return $model->resetPassword();
    }
}
