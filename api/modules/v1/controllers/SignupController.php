<?php
namespace api\modules\v1\controllers;

use yii;
use api\controllers\RestController;
use api\modules\v1\models\SignupForm;

class SignupController extends RestController
{

    public function actionOption()
    {}

    public function actionCreate()
    {
        $model = new SignupForm();
        $model->attributes = yii::$app->request->post();
        return $model->signup();
    }
}
