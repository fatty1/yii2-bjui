<?php
namespace api\modules\v1\controllers;

use yii;
use api\controllers\RestController;
use yii\filters\auth\QueryParamAuth;
use api\modules\v1\models\LogoutForm;

class LogoutController extends RestController
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className()
        ];
        return $behaviors;
    }
    
    public function actionOption()
    {}

    public function actionCreate()
    {
        $model = new LogoutForm();
        $model->attributes = yii::$app->request->post();
        $model->delToken();
        if($model->hasErrors()){
            return $model;
        }else{
            return ['message'=>'注销成功'];
        }
    }
}
