<?php
namespace api\modules\v1\controllers;

use yii;
use api\controllers\RestController;
use api\modules\v1\models\LoginForm;

class LoginController extends RestController
{

    public function actionOption()
    {}

    public function actionCreate()
    {
        $model = new LoginForm();
        $model->attributes = yii::$app->request->post();
        if ($model->validate()) {
            return $model->getToken($model->username, $model->password, $model->client);
        } else {
            return $model;
        }
    }
}
