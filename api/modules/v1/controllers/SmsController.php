<?php
namespace api\modules\v1\controllers;

use yii;
use api\controllers\RestController;
use api\modules\v1\models\SmsForm;

class SmsController extends RestController
{

    public function actionOption()
    {}

    /**
     *
     * @return \api\modules\v1\models\SmsForm|multitype:string vcode=md5(手机号+短信验证码+时间戳)
     */
    public function actionIndex()
    {
        $model = new SmsForm();
        $model->attributes = yii::$app->request->get();
        
        $vcode = rand(100, 999) . rand(100, 999);
        $model->content = '您的验证码是：' . $vcode . '。请不要把验证码泄露给其他人。如非本人操作，可不用理会！';
        $model->Send($model->mobile, $model->content);
        if ($model->hasErrors()) {
            return $model;
        } else {
            $timestamp = time() + 600;
            return [
                'message' => '发送成功',
                'verify_code' => md5($model->mobile . $vcode . $timestamp),
                'timestamp' => $timestamp
            ];
        }
    }
}
