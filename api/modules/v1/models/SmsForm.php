<?php
namespace api\modules\v1\models;

use Yii;
use yii\base\Model;

/**
 * Signup form
 */
class SmsForm extends Model
{
    public $mobile;
    public $content;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['mobile', 'filter', 'filter' => 'trim'],
            ['mobile', 'required'],
            ['mobile', 'number'],
            ['mobile', 'string', 'length' => [11, 11]],

            [['content'], 'string', 'max' => 128],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mobile' => Yii::t('app', '手机号'),
            'content' => Yii::t('app', '短信内容'),
        ];
    }

    /**
     * $url = "http://sms.106jiekou.com/utf8/sms.aspx";
     * 替换成自己的测试账号,参数顺序和wenservice对应
     * $curlPost = "account=帐号&password=接口密码&mobile=手机号码&content=" .
     *
     * rawurlencode("您的订单编码：4557。如需帮助请联系客服。");
     * echo $gets = Post($curlPost, $url);
     * 采用UTF-8编码,要将文件另存为UTF-8格式
     * 请自己解析$gets字符串并实现自己的逻辑
     * 100 表示成功,其它的参考文档
     */
    public function Send($mobile, $content)
    {
        if ($this->validate()) {
            $url = "http://sms.106jiekou.com/utf8/sms.aspx";
            $curlPost = "account=wuyousz&password=wangluo2013&mobile=$mobile&content=" . rawurlencode($content);
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_NOBODY, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $curlPost);
            $return_str = curl_exec($curl);
            curl_close($curl);
            $return = [
                '100' => '发送成功',
                '101' => '验证失败',
                '102' => '手机号码格式不正确',
                '103' => '会员级别不够',
                '104' => '内容未审核',
                '105' => '内容过多',
                '106' => '账户余额不足',
                '107' => 'Ip受限',
                '108' => '手机号码发送太频繁，请换号或隔天再发',
                '109' => '帐号被锁定',
                '110' => '发送通道不正确',
                '111' => '当前时间段禁止短信发送',
                '120' => '系统升级'
            ];
            if($return_str == '100'){
            }else{
                $this->addError('mobile', $return[$return_str]);
            }
        }
        return $this;
    }
}
