<?php

namespace api\modules\v1\models;

use Yii;
use common\models\UserToken;
use yii\base\Model;

/**
 * This is the model class for table "{{%member}}".
 *
 * @property integer $member_id
 * @property string $member_name
 * @property integer $inviter_id 
 */
class LogoutForm extends Model
{
    public $client;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['client', 'required'],
            ['client', 'string', 'max' => 10],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'client' => Yii::t('app', '客户端类型'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function delToken()
    {
        if ($this->validate()) {
            $user_id = \yii::$app->getUser()->identity->id;
            $model = UserToken::deleteAll([
                'user_id' => $user_id,
                'client_type' => $this->client
            ]);
        }
    }
}
